<div class="trigoda_gmb_stars">
	<div class="trigoda_gmb_stars_bg" style="background-color: {$trigoda_gmb_ConfigVars->getValue('trigoda_gmb_star_color_bg')};"><div class="trigoda_gmb_stars_front" date-voteaverage="{$trigoda_gmb_voteaverage}" style="background-color: {$trigoda_gmb_ConfigVars->getValue('trigoda_gmb_star_color_fg')};"></div></div>
	<div class="trigoda_gmb_stars_text">{$trigoda_gmb_text_1}<br />{$trigoda_gmb_text_2}</div>
	{if $trigoda_gmb_ConfigVars->getValue('trigoda_gmb_show_google_link') === 'on' }
	<div class="trigoda_gmb_stars_button">
		<a href="https://search.google.com/local/writereview?placeid={$trigoda_gmb_ConfigVars->getValue('trigoda_gmb_place_id')}" rel="external noopener noreferer" target="_blank">
			{$trigoda_gmb_text_3}
		</a>
	</div>
	{/if}
</div>