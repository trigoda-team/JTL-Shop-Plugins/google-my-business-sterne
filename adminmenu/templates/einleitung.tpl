<h1>Google My Business Sterne</h1>
<h2>Präsentiert von Trigoda UG</h2>
<figure><img src="{$ShopURL}/plugins/trigoda_gmb_stars/preview.png" alt="Trigoda Google My Business Sterne"></figure>
<h3>Was das Plugin bietet</h3>
<p>Binde Deine Google My Business (Unternehmensprofil) Bewertungen in Deinem Shop ein. Schnell, unkompliziert und komplett kostenlos. Du profitierst von dem Dir durch die Bewertungen entgegen gebrachten Vertrauen. Gute Bewertungen sind eine der wichtigsten Kaufempfehlungen.</p>
<h3>Dokumentation - erst lesen</h3>
<p>Du brauchst einen Google Place Api Key und ein Unternehmensprofil mit Bewertungen, damit das Plugin auf Deiner Seite funktioniert. Wie es genau funktioniert ist im Tab <b>Dokumentation</b> beschreiben, bitte lese Dir diese in Ruhe durch.</p>
<h3>Dann Einstellungen vornehmen</h3>
<p>Danach kannst Du im Tab <b>Einstellungen</b> das Plugin konfigurieren.</p>
<h3>Hinweise</h3>
<p>Das Plugin nutzt <b>JTL Shop Sprachvariablen</b>, Du kannst Sie einfach anpassen.</p>
<p><a href="{$oPlugin->getPaths()->getShopURL()}admin/pluginmanager?pluginverwaltung_uebersicht=1&sprachvariablen=1&kPlugin={$oPlugin->getID()}&token={SHOP::getAdminSessionToken()}" class="btn btn-primary">Sprachvariablen anpassen</a></p>
<p>Das Plugin nutzt das <b>JTL Shop Objektcache</b>, um die ggf. kostenpflichtigen Anfragen an die Google Paces Api gering zu halten. Daher solltest Du unbedingt den Objekt Cache aktivieren, wenn Du das Plugin nutzt! Das Plugin zeigt die Sterne auch ohne aktiviertem Cache an, aber dann kann es zu sehr vielen Google Place Api Anfragen kommen, gerade wenn die Webseite viele Besucher hat. Also, <b>nutze unbedingt das Cache</b>! Mit einer Einstellung kannst Du die Frequenz anpassen. Manchmal gibt es Situationen, wo man eine schneller Aktualisierung wünscht, dazu reicht es, die Plugin Einstellungen zu speichern - dabei wird das Cache automatisch geleert.</p>
<h3>Bei Problemen</h3>
<p>Falls Du Fragen oder Probleme mit der Nutzung des Plugins haben solltest, zögere nicht, uns zu kontaktieren, wir helfen Dir gerne weiter!</p>
<p><a href="https://trigoda.de/kontakt/" target="_blank" rel="noopener norefferer" class="btn btn-primary">Kontakt zu Triogda</a></p>
<p>Viel Spaß und maximale Erfolge mit unserem kostenlosen Open-Source Plugin!</p>